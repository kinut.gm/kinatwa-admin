import {
  Statistic,
  Row,
  Col,
  Card,
  Table,
  Typography,
  Steps,
  Result,
} from "antd";
import dynamic from "next/dynamic";
import { useQuery } from "urql";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

const { Paragraph } = Typography;

export default function Dashboard() {
  const { Step } = Steps;

  // Requests

  const GET_VEHICLES_COUNT = `
  query{
    getVehiclesCount
  }
 `;
  const GET_TRIPS_COUNT = `
  query{
    getTripsCount
  }
`;
  const GET_USERS_COUNT = `
  query {
    getUsersCount
  }
`;
  const GET_ADMIN_COUNT = `
  query {
    getAdminsCount
  }
`;
  const GET_ROUTES = `
  query{
    getRoutes{
      id
      departure
      destination
      fare
      trips{
        id
        bookings{
          payment{
            mode            
          }
        }
      }
    }
  }
`;

  const GET_VEHICLES = `
query{
  getVehicles{
    id
    registration
    trips{
      id
      route{
        fare
      }
      bookings{
        id
        payment{
          mode
        }
      }
    }
  }
}
`;

  const [{ data: vData, fetching: vFetching, error: vError }] = useQuery({
    query: GET_VEHICLES_COUNT,
  });

  const [{ data: tData, fetching: tFetching, error: tError }] = useQuery({
    query: GET_TRIPS_COUNT,
  });

  const [{ data: uData, fetching: uFetching, error: uError }] = useQuery({
    query: GET_USERS_COUNT,
  });

  const [{ data: aData, fetching: aFetching, error: aError }] = useQuery({
    query: GET_ADMIN_COUNT,
  });

  const [{ data: rData, fetching: rFetching, error: rError }] = useQuery({
    query: GET_ROUTES,
  });

  const [{ data: vData2, fetching: vFetching2, error: vError2 }] = useQuery({
    query: GET_VEHICLES,
  });

  // Configs

  const getVehiclesData = () => {
    let series = [];
    let labels = [];

    vData2?.getVehicles
      .filter((vehicle) => !vehicle.removed)
      .map((vehicle) => {
        console.log;
        labels.push(vehicle.registration);
        let trip_total = 0;
        vehicle.trips.map((trip) => {
          let trip_earning =
            trip.route.fare *
            trip.bookings.filter((booking) => booking?.payment.mode).length;
          trip_total = trip_total + trip_earning;
        });
        series.push(trip_total);
      });

    return {
      series,
      labels,
    };
  };

  let options1 = {
    chart: {
      id: "apexchart-example",
    },
    xaxis: {
      categories: getVehiclesData().labels,
    },
  };

  let series1 = [
    {
      name: "series-1",
      data: getVehiclesData().series,
    },
  ];

  const getRoutesData = () => {
    let series = [];
    let labels = [];
    rData?.getRoutes.map((route) => {
      let label = `${route.departure}-${route.destination}`;
      let serie = 0;
      route.trips?.map((trip) => {
        let trip_total =
          route.fare *
          trip.bookings.filter((booking) => booking?.payment.mode).length;
        serie = serie + trip_total;
      });
      labels.push(label);
      series.push(serie);
    });

    return {
      labels,
      series,
    };
  };

  let options2 = {
    chart: {
      width: 500,
      type: "donut",
    },
    dataLabels: {
      enabled: false,
    },

    labels: getRoutesData().labels,

    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            show: false,
          },
        },
      },
    ],
    legend: {
      position: "right",
      offsetY: 24,
      offsetX: -24,
      height: 250,
    },
  };

  if (vFetching || tFetching || uFetching || aFetching)
    return <p>Loaading...</p>;
  if (vError || tError || uError || aError) return <p>Error...</p>;

  return (
    <div
      style={{
        padding: 24,
      }}
    >
      <Row gutter={20}>
        <Col lg={6} sm={24} style={{ display: "block", width: "100%" }} md={24}>
          <Card style={{ background: "#fff", width: "100%" }}>
            <Statistic title="Shuttles" value={vData?.getVehiclesCount} />
          </Card>
        </Col>
        <Col lg={6} sm={24} style={{ display: "block", width: "100%" }}>
          <Card style={{ background: "#fff", width: "100%" }}>
            <Statistic title="Trips travelled" value={tData?.getTripsCount} />
          </Card>
        </Col>
        <Col lg={6} sm={24} style={{ display: "block", width: "100%" }}>
          <Card style={{ background: "#fff" }}>
            <Statistic
              title="Registered passengers"
              value={uData?.getUsersCount}
            />
          </Card>
        </Col>
        <Col lg={6} sm={24} style={{ display: "block", width: "100%" }}>
          <Card style={{ background: "#fff" }}>
            <Statistic title="Admins" value={aData?.getAdminsCount} />
          </Card>
        </Col>
      </Row>

      <Row gutter={24} style={{ marginTop: 24 }}>
        {/* Routes revenue */}
        <Col lg={16} sm={24}>
          <div style={{ background: "#fff", padding: 24, height: 400 }}>
            <p>Revenue by routes</p>
            <Chart
              options={options2}
              series={getRoutesData().series}
              type="donut"
              width="70%"
              height={320}
            />
          </div>
        </Col>
        <Col lg={8} sm={24}>
          <div style={{ background: "#fff", padding: 20, height: 400 }}>
            <Result status="404" subTitle="Nothing to log yet!" />
            {/* <Steps progressDot current={1} direction="vertical">
              <Step description="This is a description." />
              <Step description="This is a description." />
              <Step description="This is a description." />
            </Steps> */}
          </div>
        </Col>
      </Row>

      <Row gutter={24} style={{ marginTop: 24 }}>
        <Col span={24}>
          <div style={{ background: "#fff", padding: 24 }}>
            <p>Revenue by vehicles</p>
            <Chart
              options={options1}
              series={series1}
              type="bar"
              width="90%"
              height={320}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

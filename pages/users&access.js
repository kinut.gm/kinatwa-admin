import {
  InfoCircleOutlined,
  LeftOutlined,
  PlusCircleOutlined,
  PlusOutlined,
  RightOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import {
  Row,
  Col,
  Button,
  Input,
  Typography,
  Table,
  Modal,
  Select,
  Divider,
  Tabs,
  Form,
  DatePicker,
  Alert,
  Popover,
  Tag,
  Card,
  Statistic,
  Switch,
  TimePicker,
  Carousel,
  message,
  Avatar,
} from "antd";
import moment from "moment";
import Image from "next/image";
import { useRef } from "react";
import { useRouter } from "next/router.js";
import { useQuery, useMutation } from "urql";
import { useContext } from "react";
import { AuthContext } from "./_app.js";

const { Search } = Input;
const { Title, Text, Paragraph } = Typography;
const { OptGroup, Option } = Select;
const { TabPane } = Tabs;

export default function Admins() {
  const [user, setUser] = useContext(AuthContext);
  const ADD_ADMIN = `
    mutation ADD_ADMIN(
      $name: String
      $email: String
      $phoneNumber: String
      $username: String
      $role: String!
      $photoUrl: String
      $addedBy: ID
      $rightIds: String
    ) {
      addAdmin(
        name: $name
        email: $email
        phoneNumber: $phoneNumber
        username: $username
        role: $role
        photoUrl: $photoUrl
        addedBy: $addedBy
        rightIds: $rightIds
      ) {
        name
        username
      }
    }
  `;

  const GET_RIGHTS = `
  query {
    getRights {
      id
      label
      authorised {
        id
      }
    }
  }
  `;

  const GET_ADMINS = `
  query {
    getAdmins {
      id
      name
      username
      email
      phoneNumber
      photoUrl
      addedBy {
        username
      }
      createdAt
      rights {
        label
      }
      role
      vehicles {
        registration
        id
      }
    }
  }
  `;

  const [
    { data: adminsData, fetching: adminsFetching, error: adminsError },
    reexecuteQuery,
  ] = useQuery({
    query: GET_ADMINS,
  });

  const [{ data: rightsData, fetching: rightsFetching, error: rightsError }] =
    useQuery({
      query: GET_RIGHTS,
    });

  const [addAdminResult, _addAdmin] = useMutation(ADD_ADMIN);

  const router = useRouter();

  const [form] = Form.useForm();
  const _carousel = useRef();

  const [modal1Visible, setModal1Visible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [role, setRole] = useState(null);
  const [permissions, setPermissions] = useState([]);
  const [keyword, setKeyword] = useState("");

  const [journey, setJourney] = useState("onward");
  const [changeDriver, setChangeDriver] = useState(false);
  const [changeVehicle, setChangeVehicle] = useState(false);
  const [changeDepature, setChangeDepature] = useState(false);
  const [changeDestination, setChangeDestination] = useState(false);
  const [index, setIndex] = useState(null);

  const handleSaveUser = () => {
    let _user = {
      name: fname + " " + lname,
      username: fname.charAt(0) + fname.charAt(1) + lname,
      rightIds: permissions.toString(),
      phoneNumber: phoneNumber,
      role: role.toString(),
      addedBy: user.id,
    };

    _addAdmin(_user)
      .then((admin) => {
        console.log(admin);
        message.success(
          `Successfully added a new admin with username '${user.username}'`
        );

        setFname("");
        setLname("");
        setPhoneNumber("");
        setRole(null);
        setPermissions([]);
        setModal1Visible(false);
        reexecuteQuery({ requestPolicy: "network-only" });
      })
      .catch((err) => console.log(err));
  };

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Telephone",
      dataIndex: "phoneNumber",
    },
    {
      title: "Role",
      dataIndex: "role",
      filters: [
        {
          text: "Driver",
          value: "driver",
        },
        {
          text: "Admin",
          value: "admin",
        },
        {
          text: "Vehicle owner",
          value: "vehicle_owner",
        },
        {
          text: "Cashier",
          value: "cashier",
        },
      ],
      onFilter: (value, record) => record.role.indexOf(value) === 0,
      render: (role) => {
        return role.map((_role) => (
          <Tag key={_role} color="green">
            {_role.toUpperCase()}
          </Tag>
        ));
      },
    },
    {
      title: "Owned vehicles",
      dataIndex: "",
      sorter: (a, b) => a.vehicles.length - b.vehicles.length,
      render: (record) => {
        return <Paragraph>{record.vehicles.length}</Paragraph>;
      },
    },
    {
      title: "Member since",
      dataIndex: "",
      sorter: (a, b) => b - a,
      render: (record) => {
        return (
          <Paragraph>
            {moment(new Date(parseInt(record.createdAt))).format("MMM Do YYYY")}
          </Paragraph>
        );
      },
    },
    {
      title: "",
      dataIndex: "",
      render: (record) => {
        return (
          <Button
            type="link"
            onClick={() => {
              setIndex(adminsData?.getAdmins.indexOf(record));
              console.log(adminsData?.getAdmins.indexOf(record));
              setModal2Visible(true);
            }}
          >
            More
          </Button>
        );
      },
    },
  ];

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  if (adminsFetching) return <p>Loading ...</p>;
  if (adminsError) {
    console.log(adminsError);
    return <p>Error...</p>;
  }

  return (
    <div
      style={{
        padding: 24,
        minHeight: 360,
        background: "#fff",
      }}
    >
      <Row>
        <Col lg={12} sm={24}>
          <Button type="primary" onClick={() => setModal1Visible(true)}>
            {" "}
            <PlusCircleOutlined /> Add user
          </Button>
        </Col>
        <Col lg={12} sm={24}>
          <Input
            placeholder="Search role, name,   username"
            onChange={(e) => setKeyword(e.target.value)}
            style={{
              width: 300,
              float: "right",
            }}
            suffix={<SearchOutlined />}
          />
        </Col>
      </Row>
      <Row style={{ marginTop: 12 }}>
        <Title level={2}>
          {
            adminsData?.getAdmins.filter((admin) => {
              return (
                admin.username.toLowerCase().includes(keyword.toLowerCase()) ||
                admin.name.toLowerCase().includes(keyword.toLowerCase()) ||
                admin.role
                  .toString()
                  .toLowerCase()
                  .includes(keyword.toLowerCase())
              );
            }).length
          }
        </Title>
        <Text style={{ lineHeight: 3, marginLeft: 8 }}>users</Text>
      </Row>
      <Table
        size="small"
        scroll={{
          y: "calc(100vh - 350px)",
        }}
        pagination={false}
        columns={columns}
        dataSource={adminsData?.getAdmins.filter((admin) => {
          return (
            admin.username.toLowerCase().includes(keyword.toLowerCase()) ||
            admin.name.toLowerCase().includes(keyword.toLowerCase()) ||
            admin.role.toString().toLowerCase().includes(keyword.toLowerCase())
          );
        })}
      />

      {/* Add user modal */}
      <Modal
        visible={modal1Visible}
        footer={null}
        onCancel={() => {
          setFname("");
          setLname("");
          setPhoneNumber("");
          setRole(null);
          setPermissions([]);
          setModal1Visible(false);
        }}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          Add a new user
        </Title>
        <br />
        <Carousel ref={_carousel}>
          <div>
            <Form {...formItemLayout} form={form}>
              <Form.Item
                label="First Name"
                style={{ fontWeight: "bold", margin: 0 }}
              >
                <Input
                  placeholder="ex. Steve"
                  style={{ marginBottom: 12 }}
                  onChange={(e) => setFname(e.target.value)}
                />
              </Form.Item>
              <Form.Item
                label="Last  Name"
                style={{ fontWeight: "bold", margin: 0 }}
              >
                <Input
                  placeholder="ex. Kinuthia"
                  style={{ marginBottom: 12 }}
                  onChange={(e) => setLname(e.target.value)}
                />
              </Form.Item>
              <Form.Item
                label="Telephone"
                style={{ fontWeight: "bold", margin: 0 }}
              >
                <Input
                  placeholder="ex. 0748920306"
                  style={{ marginBottom: 12 }}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                />
              </Form.Item>
              <Form.Item label="Role" style={{ fontWeight: "bold", margin: 0 }}>
                <Select
                  mode="multiple"
                  allowClear
                  placeholder="ex. Driver"
                  onChange={(val) => {
                    setRole(val);
                  }}
                  style={{ fontWeight: "400" }}
                >
                  <Option value="driver">Driver</Option>
                  <Option value="admin">Admin</Option>
                  <Option value="cashier">Cashier</Option>
                  <Option value="vehicle_owner">Vehicle owner</Option>
                </Select>
              </Form.Item>
            </Form>
            <Button
              onClick={() => {
                _carousel.current.next();
              }}
              type="primary"
              style={{ marginTop: 12, float: "right" }}
            >
              Set up rights <RightOutlined />
            </Button>
          </div>
          <div>
            <p>This user will have the following rights :</p>
            <div>
              {rightsData?.getRights.map((right) => (
                <span
                  key={right.id}
                  style={{ display: "block", margin: "8px 0px" }}
                >
                  <Switch
                    size="small"
                    style={{ marginRight: 6 }}
                    onChange={(val) => {
                      if (val == true) {
                        setPermissions([...permissions, right.id]);
                        return;
                      } else {
                        setPermissions([
                          ...permissions.filter(
                            (permission) => permission !== right.id
                          ),
                        ]);
                        return;
                      }
                    }}
                  />
                  {right.label
                    .split("_")
                    .map((word, inx) =>
                      inx == 0
                        ? word.charAt(0).toUpperCase() + word.slice(1) + " "
                        : word + " "
                    )}
                </span>
              ))}
            </div>

            <Row
              style={{
                justifyContent: "space-between",
                position: "absolute",
                bottom: "0.3rem",
              }}
            >
              <Button
                onClick={() => _carousel.current.prev()}
                type="primary"
                style={{ marginTop: 12, float: "left" }}
              >
                <LeftOutlined /> Back
              </Button>

              <Button
                type="primary"
                onClick={handleSaveUser}
                style={{ marginTop: 12, marginLeft: 12 }}
              >
                Add user
              </Button>
            </Row>
          </div>
        </Carousel>
      </Modal>

      {/* More about user modal */}
      <Modal
        visible={modal2Visible}
        onOk={null}
        footer={null}
        onCancel={() => setModal2Visible(false)}
      >
        <Title level={3}>User details</Title>
        {index && adminsData ? (
          <Tabs>
            <TabPane tab="Info" key="1">
              <Row>
                <Col span={6}>
                  {!adminsData?.getAdmins[index].photoUrl ? (
                    <Avatar
                      size={80}
                      style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                    >
                      {adminsData?.getAdmins[index].name
                        .split("")[0]
                        .charAt(0) +
                        adminsData?.getAdmins[index].name
                          .split("")[1]
                          .charAt(0)
                          .toUpperCase()}
                    </Avatar>
                  ) : (
                    <Image
                      height={80}
                      width={80}
                      style={{ borderRadius: 40 }}
                      src={""}
                    />
                  )}
                </Col>
                <Col span={18}>
                  <Form {...formItemLayout} form={form}>
                    <Form.Item
                      label="Username"
                      style={{ fontWeight: "bold", margin: 0 }}
                    >
                      <p style={{ fontWeight: "400" }}>
                        {adminsData?.getAdmins[index].username}
                      </p>
                    </Form.Item>
                    <Form.Item
                      label="Name"
                      style={{ fontWeight: "bold", margin: 0 }}
                    >
                      <p style={{ fontWeight: "400" }}>
                        {adminsData?.getAdmins[index].name}
                      </p>
                    </Form.Item>
                    <Form.Item
                      label="Telephone"
                      style={{ fontWeight: "bold", margin: 0 }}
                    >
                      <p style={{ fontWeight: "400" }}>
                        {adminsData?.getAdmins[index].phoneNumber}
                      </p>
                    </Form.Item>
                    <Form.Item
                      label="Vehicles owned"
                      style={{ fontWeight: "bold", margin: 0 }}
                    >
                      <p style={{ fontWeight: "400" }}>
                        {adminsData?.getAdmins[index].vehicles.length}
                      </p>
                    </Form.Item>
                    <Form.Item
                      label="Member since"
                      style={{ fontWeight: "bold", margin: 0 }}
                    >
                      <p style={{ fontWeight: "400" }}>
                        {moment(
                          new Date(
                            parseInt(adminsData?.getAdmins[index].createdAt)
                          )
                        ).format("MMMM Do YYYY")}
                      </p>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="Rights" key="2">
              This user has the following rights :
              <div style={{ marginTop: 12 }}>
                {rightsData?.getRights.map((right) => (
                  <span
                    key={right.id}
                    style={{ display: "block", margin: "8px 0px" }}
                  >
                    <Switch
                      size="small"
                      disabled
                      checked={
                        right.authorised.filter(
                          (admin) => admin.id == admins[index].id
                        ).length < 1
                          ? false
                          : true
                      }
                      style={{ marginRight: 6 }}
                    />
                    {right.label
                      .split("_")
                      .map((word, inx) =>
                        inx == 0
                          ? word.charAt(0).toUpperCase() + word.slice(1) + " "
                          : word + " "
                      )}
                  </span>
                ))}
              </div>
              <Button type="primary" block style={{ marginTop: 24 }}>
                Save changes
              </Button>
            </TabPane>
            <TabPane tab="Remove User" key="3">
              <Paragraph>
                <Alert message="Warning" banner />
                <br />
                Are you sure you want to remove this user permanently from the
                system ?
                <br />
                <span style={{ marginTop: 12 }}>
                  Type your passsword below to remove user.
                </span>
              </Paragraph>
              <Input value={null} />

              <Button type="primary" block style={{ marginTop: 24 }}>
                Remove user
              </Button>
            </TabPane>
          </Tabs>
        ) : null}
      </Modal>
    </div>
  );
}

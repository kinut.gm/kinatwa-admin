import {
  Button,
  Row,
  Card,
  Col,
  Form,
  Modal,
  Typography,
  Tabs,
  Divider,
  Result,
  message,
  Empty,
  Input,
  InputNumber,
} from "antd"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "urql"
import { useState, useEffect } from "react"
import style from "../styles/Styles.module.css"
import moment from "moment"
import { InfoCircleFilled } from "@ant-design/icons"

const { Text, Title } = Typography
const { TabPane } = Tabs

export default function Route({ routes }) {
  const router = useRouter()

  const [form] = Form.useForm()

  // States
  const [newRouteModal, setNewRouteModal] = useState(false)
  const [fare, setFare] = useState(null)
  const [duration, setDuration] = useState(null)
  const [departure, setDeparture] = useState("")
  const [hours, setHours] = useState(null)
  const [minutes, setMinutes] = useState(null)
  const [destination, setDestination] = useState("")

  // Requests
  const GET_ROUTES = ` 
   query {
    getRoutes {
      id
      destination
      departure
      duration
      fare
      createdAt
      removed
      trips {
        id
        journeyType
      }
    }
  }`

  const ADD_ROUTE = `
    mutation ADD_ROUTES($departure:String, $destination:String, $duration:Int, $fare:Int){
      addRoute(departure:$departure, destination:$destination, duration:$duration, fare:$fare){
        id
        departure
        destination
      }
    }
  `

  const [{ data, fetching, error }, reexecuteQuery] = useQuery({
    query: GET_ROUTES,
  })

  const [addRouteResult, _addRoute] = useMutation(ADD_ROUTE)

  // Functions
  useEffect(() => {
    let duration = hours * 3600 + minutes * 60
    setDuration(duration)
  }, [hours, minutes])

  const addRoute = () => {
    const payload = {
      departure,
      destination,
      fare,
      duration,
    }
    _addRoute(payload)
      .then((result) => {
        console.log(result)
        message.success(
          `Successfully added a new route: '${departure} - ${destination}'`
        )
        setDeparture("")
        setDestination("")
        setDuration(null)
        setHours(null)
        setMinutes(null)
        setFare(null)
        setNewRouteModal(false)
        reexecuteQuery({ requestPolicy: "network-only" })
      })
      .catch((err) => {
        message.error(`An error occured`)
      })
  }

  if (fetching) return <p>Loading</p>
  if (error) return <p>Error</p>

  // Configs
  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  }

  const { Title } = Typography

  return (
    <div
      style={{
        padding: 24,
        minHeight: 360,
        background: "#fff",
      }}
    >
      {data?.getRoutes.filter((route) => route.removed == false).length == 0 ? (
        <Result
          icon={<Empty />}
          title="Set up routes to get started!"
          extra={
            <Button type="primary" onClick={() => setNewRouteModal(true)}>
              Add route
            </Button>
          }
        />
      ) : (
        <>
          <Row justify="space-between">
            <>
              <Title level={2}>
                {
                  data?.getRoutes.filter((route) => route.removed == false)
                    .length
                }
                <span
                  style={{
                    fontWeight: "400",
                    fontSize: "0.85rem",
                    marginLeft: 4,
                  }}
                >
                  route
                </span>
              </Title>

              <Button type="primary" onClick={() => setNewRouteModal(true)}>
                Add a route
              </Button>
            </>
          </Row>

          <Row gutter={24}>
            {data?.getRoutes
              .filter((route) => route.removed == false)
              .map((route) => (
                <Col key={route.id} lg={8} sm={24} md={8}>
                  <RouteCard
                    data={route}
                    key={route.id}
                    routes={data.getRoutes.filter(
                      (route) => route.removed == false
                    )}
                    refresh={() =>
                      reexecuteQuery({ requestPolicy: "network-only" })
                    }
                  />
                </Col>
              ))}
          </Row>
        </>
      )}

      <Modal
        visible={newRouteModal}
        onOk={addRoute}
        onCancel={() => {
          setDeparture("")
          setDestination("")
          setDuration(null)
          setHours(null)
          setMinutes(null)
          setFare(null)
          setNewRouteModal(false)
        }}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          Add a new route
        </Title>
        <br />
        <Form {...formItemLayout} form={form}>
          <Form.Item
            label="Departure"
            style={{ fontWeight: "bold", margin: 0 }}
          >
            <Input
              placeholder="ex. Nairobi"
              style={{ marginBottom: 12 }}
              onChange={(e) => setDeparture(e.target.value)}
            />
          </Form.Item>
          <Form.Item
            label="Destination"
            style={{ fontWeight: "bold", margin: 0 }}
          >
            <Input
              placeholder="ex. Kitui"
              style={{ marginBottom: 12 }}
              onChange={(e) => setDestination(e.target.value)}
            />
          </Form.Item>
          <Form.Item label="Duration" style={{ fontWeight: "bold", margin: 0 }}>
            <Row>
              <InputNumber
                style={{ marginRight: 12 }}
                min={0}
                max={24}
                onChange={(val) => setHours(val)}
              />
              <span style={{ marginRight: 12 }}>Hours</span>
              <InputNumber
                style={{ marginRight: 12 }}
                min={0}
                max={59}
                onChange={(val) => setMinutes(val)}
              />
              <span style={{ marginRight: 12 }}>Minutes</span>
            </Row>
          </Form.Item>
          <Form.Item label="Fare" style={{ fontWeight: "bold", margin: 0 }}>
            <InputNumber
              addonAfter="KSH"
              block
              placeholder="ex. 1200"
              style={{ marginBottom: 12, width: "100%" }}
              onChange={(val) => setFare(val)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

const RouteCard = ({ data, routes, refresh }) => {
  const { Title, Paragraph } = Typography

  // States
  const [moreModal, setMoreModal] = useState(false)
  const [index, setIndex] = useState(0)
  const [newFare, setNewFare] = useState(null)
  const [password, setPassword] = useState("")

  // Requests
  const UPDATE_ROUTE = `
    mutation UPDATE_ROUTE($id: ID!, $fare: Int, $removed: Boolean) {
      updateRoute(id: $id, fare: $fare, removed: $removed) {
        id
        fare
      }
    }
  `

  const [updateFareResult, _updateRoute] = useMutation(UPDATE_ROUTE)

  // Functions
  function secondsToHms(d) {
    d = Number(d)
    var h = Math.floor(d / 3600)
    var m = Math.floor((d % 3600) / 60)
    var s = Math.floor((d % 3600) % 60)

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : ""
    var mDisplay = m + (m == 1 ? " minute " : " minutes ")

    return hDisplay + mDisplay
  }

  const modifyFare = () => {
    _updateRoute({
      id: newFare.routeId,
      fare: Number(newFare.newFare),
    })
      .then(() => {
        message.success("Price of fare successfully updated")
        setMoreModal(false)
        setNewFare(null)
        refresh()
      })
      .catch((err) => message.error("An unexpected error occured"))
  }

  const deleteRoute = (id) => {
    _updateRoute({
      id,
      removed: true,
    })
      .then(() => {
        message.info(`Route deleted!`)
        setMoreModal(false)
        setPassword("")
        setNewFare(null)
        refresh()
      })
      .catch((err) => message.error("An unexpected error occured"))
  }

  return (
    <>
      <Card
        size="small"
        style={{ width: "100%", marginBottom: 24 }}
        cover={<img src="/bg.png" alt="bg" />}
      >
        <Row
          style={{ marginTop: -60, marginBottom: 20 }}
          justify="space-between"
        >
          <Title level={3} style={{ color: "#001529", letterSpacing: -0.8 }}>
            {`${data.departure} - ${data.destination}`}
          </Title>
          <Button
            type="primary"
            onClick={() => {
              setMoreModal(true)
              setIndex(routes.indexOf(data))
            }}
          >
            More
          </Button>
        </Row>
        <p style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}>
          Duration :
          <span style={{ fontWeight: "400", float: "right" }}>
            {secondsToHms(Number(data.duration))}
          </span>
        </p>
        <p style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}>
          Fare :
          <span style={{ fontWeight: "400", float: "right" }}>
            {" " + data.fare + " " + "KES"}
          </span>
        </p>
        <p style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}>
          Trips :
          <span style={{ fontWeight: "400", float: "right" }}>
            {" " + data.trips.length}
          </span>
        </p>

        <p style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}>
          Added on :
          <span style={{ fontWeight: "400", float: "right" }}>
            {" " +
              moment(new Date(parseInt(data.createdAt))).format("MMM Do YYYY")}
          </span>
        </p>
        <p style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}>
          Added by :
          <span style={{ fontWeight: "400", float: "right" }}>
            {" " + data.trips.length}
          </span>
        </p>
      </Card>

      {/* More Modal */}
      <Modal
        visible={moreModal}
        footer={null}
        onCancel={() => setMoreModal(false)}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          {" "}
          {routes[index].departure + " - " + routes[index].destination}
        </Title>

        <Tabs>
          <TabPane tab="Info" key="1">
            <div style={{ width: "90%" }}>
              <p
                style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}
              >
                Duration :
                <span style={{ fontWeight: "400", float: "right" }}>
                  {secondsToHms(routes[index].duration)}
                </span>
              </p>
              <p
                style={{
                  fontWeight: "bold",
                  margin: "10px 0px",
                  width: "100%",
                }}
              >
                Fare :
                <span style={{ fontWeight: "400", float: "right" }}>
                  <Paragraph
                    editable={{
                      onChange: (val) =>
                        setNewFare({
                          routeId: routes[index].id,
                          newFare: val,
                        }),
                    }}
                  >
                    {" "}
                    {newFare ? (
                      <span
                        style={{
                          fontWeight: "bold",
                          color: "orange",
                          margin: 0,
                        }}
                      >
                        {newFare.newFare}
                      </span>
                    ) : (
                      routes[index].fare
                    )}{" "}
                    KES
                  </Paragraph>
                </span>
              </p>
              <Divider />
              <p
                style={{
                  fontWeight: "bold",
                  margin: "10px 0px",
                  width: "100%",
                }}
              >
                Trips (onwards) :
                <span style={{ fontWeight: "400", float: "right" }}>
                  {" " +
                    routes[index].trips.filter(
                      (trip) => trip.journeyType == "onward"
                    ).length}
                </span>
              </p>
              <p
                style={{
                  fontWeight: "bold",
                  margin: "10px 0px",
                  width: "100%",
                }}
              >
                Trips (returns) :
                <span style={{ fontWeight: "400", float: "right" }}>
                  {" " +
                    routes[index].trips.filter(
                      (trip) => trip.journeyType == "return"
                    ).length}
                </span>
              </p>
              <p
                style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}
              >
                Added on :
                <span style={{ fontWeight: "400", float: "right" }}>
                  {" " +
                    moment(new Date(parseInt(routes[index].createdAt))).format(
                      "MMM Do YYYY"
                    )}
                </span>
              </p>
              <p
                style={{ fontWeight: "bold", margin: "6px 0px", width: "100%" }}
              >
                Added by :
                <span style={{ fontWeight: "400", float: "right" }}>
                  {" " + routes[index].trips.length}
                </span>
              </p>
            </div>

            {newFare && newFare.newFare !== routes[index].fare && (
              <Button
                type="primary"
                block
                style={{ marginTop: 24 }}
                onClick={modifyFare}
              >
                {" "}
                Save changes{" "}
              </Button>
            )}
          </TabPane>
          <TabPane tab="Trips" key="2">
            <Result
              status="404"
              title="Missing"
              subTitle="Sorry, page still in development."
            />
          </TabPane>
          <TabPane tab="Stats" key="3">
            <Result
              status="404"
              title="Missing"
              subTitle="Sorry, page still in development."
            />
          </TabPane>
          <TabPane tab="Delete" key="4">
            <Paragraph>
              <InfoCircleFilled style={{ color: "orange" }} /> Do you want to
              delete this route ? If there are existing trips gone or in booking
              , their records will still remain. Type your password below to
              confirm this deletion
            </Paragraph>
            <Input.Password
              block
              onChange={(e) => setPassword(e.target.value)}
            />

            <Button
              type="primary"
              style={{ marginTop: 12 }}
              block
              onClick={() => deleteRoute(routes[index].id)}
            >
              Delete route
            </Button>
          </TabPane>
        </Tabs>
      </Modal>
    </>
  )
}

import {
  Result,
  Avatar,
  Row,
  Col,
  Divider,
  Form,
  Typography,
  Table,
  Tabs,
} from "antd";
import { useContext } from "react";
import { AuthContext } from "./_app.js";
import { useQuery, useMutation } from "urql";
import moment from "moment";

const { Paragraph, Title } = Typography;
const { TabPane } = Tabs;

export default function Profile() {
  const [user, setUser] = useContext(AuthContext);

  const GET_USER = `
    query($id:ID!){
      getAdmin(id:$id){
        name
        username
        role
        email
        phoneNumber
        createdAt
        password
        addedBy{
          id
          name
          username
        }
        rights{
          label
          id
        }
        vehicles{
          id
          registration
          id
          primaryRoute{
            departure
            destination
          }
          driver{
            name
          }
          createdAt
        }
      }
    }
  `;

  const [{ data: uData, fetching: uFetching, error: uError }, reexecuteQuery] =
    useQuery({
      query: GET_USER,
      variables: {
        id: user.id,
      },
    });

  const { Paragraph } = Typography;

  const [form] = Form.useForm();
  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  if (uFetching) return <p>Loading...</p>;
  if (uError) return <p>Error...</p>;

  return (
    <div
      style={{
        padding: 24,
        background: "#fff",
      }}
    >
      <Row>
        <Col sm={24} lg={4}>
          <Avatar
            size={150}
            style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
          >
            SK
          </Avatar>
        </Col>

        <Col sm={24} lg={14}>
          <br />
          <Form
            {...formItemLayout}
            form={form}
            style={{ width: "100%", marginTop: 12 }}
          >
            <Row>
              <Col sm={24} lg={6} style={{ width: "100%" }}>
                <Form.Item
                  label="Username"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Paragraph style={{ fontWeight: "normal" }} editable>
                    {uData?.getAdmin.username}
                  </Paragraph>
                </Form.Item>
              </Col>{" "}
              <Col sm={24} lg={6} style={{ width: "100%" }}>
                <Form.Item
                  label="Name"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Paragraph style={{ fontWeight: "normal" }} editable>
                    {uData?.getAdmin.name}
                  </Paragraph>
                </Form.Item>
              </Col>
              <Col sm={24} lg={6} style={{ width: "100%" }}>
                <Form.Item
                  label="Added on"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Paragraph style={{ fontWeight: "normal" }}>
                    {moment(new Date(Number(uData?.getAdmin.createdAt))).format(
                      "MMM Do YYYY"
                    )}
                  </Paragraph>
                </Form.Item>
              </Col>
              <Col sm={24} lg={6} style={{ width: "100%" }}>
                <Form.Item
                  label="Added by"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Paragraph style={{ fontWeight: "normal" }}>
                    {uData?.getAmin?.addedBy?.name}
                  </Paragraph>
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <br />
        </Col>
      </Row>
      <Divider orientation="right">My vehicles</Divider>
      <div>
        {uData?.getAdmin.vehicles.length < 1 ? (
          <Result
            status="404"
            title={`Looks like you don't own any vehicles`}
          />
        ) : (
          <_Vehicles data={uData} />
        )}
      </div>
    </div>
  );
}

const _Vehicles = ({ data: uData }) => {
  const columns = [
    {
      title: "Registration",
      dataIndex: "registration",
    },
    {
      title: "Primary route",
      dataIndex: "",
      render: (record) => {
        return (
          <Paragraph>{`${record?.primaryRoute?.departure} - ${record?.primaryRoute?.destination} `}</Paragraph>
        );
      },
    },
    {
      title: "Driver",
      dataIndex: "",
      render: (record) => {
        return <Paragraph>{record?.driver?.name}</Paragraph>;
      },
    },
    {
      title: "Added on",
      dataIndex: "",
      render: (record) => {
        return (
          <Paragraph>
            {new Date(parseInt(record.createdAt)).toDateString()}
          </Paragraph>
        );
      },
    },
  ];
  return (
    <>
      <Table
        size="small"
        pagination={false}
        columns={columns}
        scroll={{
          y: "calc(100vh - 350px)",
        }}
        dataSource={uData?.getAdmin.vehicles}
      />
    </>
  );
};

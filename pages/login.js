import { Form, Input, Button } from "antd";
import { useState } from "react";

const formItemLayout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};

export default function Login() {
  const [form] = Form.useForm();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <>
      <div style={{ background: "#63C34A", height: "100vh", width: "100vw" }}>
        <Form
          {...formItemLayout}
          form={form}
          style={{ background: "#fff", padding: 24 }}
        >
          <Form.Item label="Username" style={{ fontWeight: "bold", margin: 0 }}>
            <Input onChange={(e) => setUsername(e.target.value)} />
          </Form.Item>
          <Form.Item label="Username" style={{ fontWeight: "bold", margin: 0 }}>
            <Input.Password onChange={(e) => setPassword(e.target.value)} />
          </Form.Item>
          <Button type="primary" block style={{ marginTop: 12 }}>
            Login
          </Button>
        </Form>
      </div>
    </>
  );
}

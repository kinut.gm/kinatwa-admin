import { PlusCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { useRouter } from "next/router.js";
import { useQuery, useMutation } from "urql";
import { useState } from "react";

import {
  Row,
  Col,
  Button,
  Input,
  Typography,
  Table,
  Modal,
  Select,
  Space,
  Divider,
  Tabs,
  Form,
  Radio,
  Result,
  DatePicker,
  Popover,
  Tag,
  Alert,
  Card,
  Empty,
  Statistic,
  TimePicker,
  Popconfirm,
  message,
} from "antd";
import moment from "moment";
import Image from "next/image";

const { Search } = Input;
const { Title, Text, Paragraph } = Typography;
const { OptGroup, Option } = Select;
const { TabPane } = Tabs;

export default function Trips() {
  const [form] = Form.useForm();
  const router = useRouter();

  // States
  const [modal1Visible, setModal1Visible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [changeDriver, setChangeDriver] = useState(false);
  const [changeVehicle, setChangeVehicle] = useState(false);
  const [changeDeparture, setChangeDeparture] = useState(false);
  const [changeDestination, setChangeDestination] = useState(false);

  const [route, setRoute] = useState("");
  const [driver, setDriver] = useState("");
  const [vehicle, setVehicle] = useState("");
  const [departureTime, setDepartureTime] = useState("");
  const [journey, setJourney] = useState("onward");

  const [newDriver, setNewDriver] = useState("");
  const [newVehicle, setNewVehicle] = useState("");
  const [newDepartureTime, setNewDepartureTime] = useState("");

  const [keyword, setKeyword] = useState("");

  const [index, setIndex] = useState(-1);

  // Requests
  const CREATE_TRIP = `
    mutation CREATE_TRIP(
      $route: ID
      $departureTime: String
      $journeyType: String
      $vehicle: ID
      $driver: ID
      $createdBy: ID
    ) {
      createTrip(
        createdBy: $createdBy
        departureTime: $departureTime
        driver: $driver
        journeyType: $journeyType
        route: $route
        vehicle: $vehicle
      ) {
        id
      }
    }
  `;

  const BOOK_TICKET = `
    mutation BOOK_TICKET(
      $trip: ID
    ){
      bookTicket(trip:$trip){
        id
        seatNo
        passenger{
          name
          age
        }
        payment{
          mode
          timestamp
        }      
      }
    }
  `;

  const UPDATE_TRIP = `
    mutation UPDATE_TRIP(
      $id: ID
      $departureTime: String
      $vehicle: ID
      $driver: ID
    ){
      updateTrip(
        id:$id
        departureTime: $departureTime
        vehicle: $vehicle
        driver: $driver
      ){
        id        
      }
    }
  `;

  const GET_TRIPS = `
  query {
    getTrips {
      id
      driver {
        id
        name
      }
      route {
        departure
        destination
        duration
        fare
      }
      vehicle {
        id
        registration
        seats
      }
      cancelled
      departureTime
      journeyType
      bookings {
        id
        seatNo
        passenger{
          name
          age
          gender
        }
        payment{
          mode
          tx_ref
          timestamp
        }
      }
    }
  }
  `;

  const GET_ROUTES = `
  query {
    getRoutes {
      id
      destination
      departure
      duration
      vehicles {
        id
        registration
        createdAt
        driver {
          id
          name
        }
      }
    }
  }
  `;

  const GET_ADMINS = `
  query {
    getAdmins {
      id
      createdAt
      name
      role
    }
  }
  `;

  const [createTripResult, _createTrip] = useMutation(CREATE_TRIP);
  const [updateTripResult, _updateTrip] = useMutation(UPDATE_TRIP);
  const [bookTicketResult, _bookTicket] = useMutation(BOOK_TICKET);

  const [{ data: tData, fetching: tFetching, error: tError }, reexecuteQuery] =
    useQuery({
      query: GET_TRIPS,
    });

  const [{ data: rData, fetching: rFetching, error: rError }] = useQuery({
    query: GET_ROUTES,
  });

  const [{ data: aData, fetching: aFetching, error: aError }] = useQuery({
    query: GET_ADMINS,
  });

  // Functions
  const createTrip = () => {
    _createTrip({
      route,
      departureTime,
      journeyType: journey,
      vehicle,
      driver,
      createdBy: driver,
    }).then(() => {
      message.success("Trip successfully created !");
      setDriver("");
      setVehicle("");
      setJourney("onward");
      setRoute("");
      setDepartureTime("");
      setModal1Visible(false);
      reexecuteQuery({ requestPolicy: "network-only" });
    });
  };

  const updateTrip = (id) => {
    const payload = {
      id,
      driver: newDriver,
      vehicle: newVehicle,
      departureTime: newDepartureTime,
    };

    Object.keys(payload).forEach((key) => {
      if (payload[key] === "") {
        delete payload[key];
      }
    });

    console.log(payload);

    _updateTrip(payload).then(() => {
      message.success("Trip details updated!");
      setNewDriver("");
      setNewVehicle("");
      setNewDepartureTime("");
      setModal2Visible(false);
      setChangeDriver(false);
      setChangeVehicle(false);
      setChangeDeparture(false);
      reexecuteQuery({ requestPolicy: "network-only" });
    });
  };

  const bookTicket = (id) => {
    let payload = {
      trip: id,
    };
    _bookTicket(payload).then((res) => {
      if (!res.data.bookTicket) {
        message.error("Trip fully booked. Create a new trip");
      }
    });
  };

  // Configs
  const columns = [
    {
      title: "Vehicle",
      dataIndex: "",
      render: (record) => {
        return <Paragraph>{record.vehicle.registration}</Paragraph>;
      },
    },
    {
      title: "Driver",
      dataIndex: "",
      render: (record) => {
        return <Paragraph>{record.driver.name}</Paragraph>;
      },
    },
    {
      title: "Departure",
      dataIndex: "",
      render: (record) => {
        return record.journeyType == "onward" ? (
          <Paragraph>{record.route.departure}</Paragraph>
        ) : (
          <Paragraph>{record.route.destination}</Paragraph>
        );
      },
    },
    {
      title: "Destination",
      dataIndex: "",
      render: (record) => {
        return record.journeyType == "onward" ? (
          <Paragraph>{record.route.destination}</Paragraph>
        ) : (
          <Paragraph>{record.route.departure}</Paragraph>
        );
      },
    },
    {
      title: "Boarding",
      dataIndex: "",
      width: 150,
      render: (record) => {
        return (
          <Paragraph>
            {moment(new Date(Number(record.departureTime) * 1000)).format(
              "Do MMM YY | h:mm a"
            )}
          </Paragraph>
        );
      },
    },
    {
      title: "Arrival",
      dataIndex: "",
      width: 170,
      render: (record) => {
        return (
          <Paragraph>
            {moment(
              new Date(
                Number(record.departureTime) * 1000 +
                  record.route.duration * 1000
              )
            ).format("Do MM YY | h:mm a")}
          </Paragraph>
        );
      },
    },
    {
      title: "Status",
      dataIndex: "",
      render: (record) => {
        let availableSeats = record.bookings.filter(
          (booking) => !booking.payment?.mode
        ).length;
        return (
          <Tag
            color={
              availableSeats == 0
                ? "green"
                : availableSeats < 5
                ? "orange"
                : availableSeats > 9
                ? "blue"
                : "yellow"
            }
          >
            {availableSeats == 0 ? "FULLY BOOKED" : `${availableSeats} LEFT`}
          </Tag>
        );
      },
    },
    {
      title: "",
      dataIndex: "",
      render: (record) => {
        return (
          <Button
            type="link"
            onClick={() => {
              setModal2Visible(true);
              setIndex(tData?.getTrips.indexOf(record));
            }}
          >
            More
          </Button>
        );
      },
    },
  ];

  const seatsInfo = [
    {
      seat: 1,
      payment: {
        tx_ref: "QRPSPRN001",
        date: moment().format("MMM Do YYYY | h:mm a"),
      },
      customer: {
        name: "Steve Kinuthia",
        phoneNumber: "0748920306",
        age: 21,
        idNumber: "38213846",
      },
    },
    {
      seat: 2,
      payment: null,
      customer: null,
    },
  ];

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  return (
    <div
      style={{
        padding: 24,
        minHeight: 360,
        background: "#fff",
      }}
    >
      <Row>
        <Col lg={12} sm={24}>
          <Button type="primary" onClick={() => setModal1Visible(true)}>
            {" "}
            <PlusCircleOutlined /> Create trip
          </Button>
        </Col>
        <Col lg={12} sm={24}>
          <Input
            suffix={<SearchOutlined />}
            placeholder="Search registration, driver"
            style={{
              width: 300,
              float: "right",
            }}
            onChange={(e) => setKeyword(e.target.value)}
          />
        </Col>
      </Row>
      <Row style={{ marginTop: 12 }}>
        <Title level={2}>
          {
            tData?.getTrips.filter(
              (trip) =>
                !trip.cancelled &&
                (trip.driver.name
                  .toLowerCase()
                  .includes(keyword.toLowerCase()) ||
                  trip.vehicle.registration
                    .toLowerCase()
                    .includes(keyword.toLowerCase()))
            ).length
          }
        </Title>
        <Text style={{ lineHeight: 3, marginLeft: 8 }}>trips</Text>
      </Row>
      {tData?.getTrips.filter((trip) => !trip.cancelled).length == 0 ? (
        <Result
          icon={<Empty />}
          title="Create a trip to get started"
          extra={
            <Button type="primary" onClick={() => setModal1Visible(true)}>
              Create trip
            </Button>
          }
        />
      ) : (
        <Table
          size="small"
          scroll={{
            y: "calc(100vh - 350px)",
          }}
          pagination={false}
          columns={columns}
          dataSource={tData?.getTrips.filter(
            (trip) =>
              !trip.cancelled &&
              (trip.driver.name.toLowerCase().includes(keyword.toLowerCase()) ||
                trip.vehicle.registration
                  .toLowerCase()
                  .includes(keyword.toLowerCase()))
          )}
        />
      )}
      {/* Create trip modal */}
      <Modal
        visible={modal1Visible}
        okText="Create trip"
        onOk={createTrip}
        onCancel={() => {
          setDriver("");
          setVehicle("");
          setJourney("onward");
          setRoute("");
          setDepartureTime("");
          setModal1Visible(false);
        }}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          Create trip
        </Title>
        <br />
        <Select
          style={{
            width: "100%",
            marginBottom: 16,
          }}
          onChange={(val) => setRoute(val)}
          placeholder="Primary route"
        >
          {rData?.getRoutes.map((route) => (
            <Option value={route.id} key={route.id}>
              {route.departure} - {route.destination}
            </Option>
          ))}
        </Select>
        <Radio.Group
          style={{ marginBottom: 24, marginLeft: 24 }}
          onChange={(e) => setJourney(e.target.value)}
          value={journey}
        >
          <Space direction="vertical">
            <Radio value={"onward"}>Onward journey</Radio>
            <Radio value={"return"}>Return journey</Radio>
          </Space>
        </Radio.Group>
        <br />
        <DatePicker
          placeholder="Departure time"
          use12Hours
          showTime
          onOk={(val) => setDepartureTime(String(val.unix()))}
          style={{ marginBottom: 12, width: "100%" }}
        />
        <br />
        <Select
          style={{
            width: "100%",
            marginBottom: 16,
          }}
          onChange={(val) => setVehicle(val)}
          placeholder="Vehicle"
        >
          {rData?.getRoutes.map((route) => (
            <OptGroup
              key={route.id}
              label={`${route.departure} - ${route.destination}`}
            >
              {route.vehicles.map((vehicle) => (
                <Option key={vehicle.id} value={vehicle.id}>
                  {vehicle.registration}
                </Option>
              ))}
            </OptGroup>
          ))}
        </Select>
        <Select
          style={{
            width: "100%",
            marginBottom: 16,
          }}
          onChange={(val) => setDriver(val)}
          placeholder="Driver"
        >
          <OptGroup label="Drivers">
            {aData?.getAdmins
              .filter((admin) => admin.role.includes("driver"))
              .map((driver) => (
                <Option key={driver.id} value={driver.id}>
                  {driver.name}
                </Option>
              ))}
          </OptGroup>
        </Select>
      </Modal>

      {/* More about trip */}
      <Modal
        visible={modal2Visible}
        onOk={null}
        width="70%"
        footer={null}
        onCancel={() => setModal2Visible(false)}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          Trip details
        </Title>
        <br />
        {tData?.getTrips.length > 0 && index > -1 ? (
          <Tabs>
            <TabPane tab="Info" key="1">
              <Form {...formItemLayout} form={form}>
                <Form.Item
                  label="Driver"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Row>
                    <Col span={8}>
                      {" "}
                      <Paragraph
                        style={{ lineHeight: 2.2, fontWeight: "normal" }}
                      >
                        {tData?.getTrips[index].driver.name}
                      </Paragraph>
                    </Col>

                    <Col span={8}>
                      {changeDriver && (
                        <Select
                          style={{
                            fontWeight: "400",
                          }}
                          placeholder="New driver"
                          onChange={(val) => setNewDriver(val)}
                        >
                          {aData?.getAdmins
                            .filter(
                              (admin) =>
                                admin.role.includes("driver") &&
                                admin.id !== tData?.getTrips[index].driver.id
                            )
                            .map((driver) => (
                              <Option key={driver.id} value={driver.id}>
                                {driver.name}
                              </Option>
                            ))}
                        </Select>
                      )}
                    </Col>

                    <Col offset={4}>
                      {" "}
                      <Typography.Link
                        onClick={() => setChangeDriver(!changeDriver)}
                        style={{
                          whiteSpace: "nowrap",
                          fontWeight: "normal",
                          fontSize: "0.7rem",
                          float: "right",
                        }}
                      >
                        Change driver
                      </Typography.Link>
                    </Col>
                  </Row>
                </Form.Item>
                <Form.Item
                  label="Vehicle"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Row>
                    <Col span={8}>
                      {" "}
                      <Paragraph
                        style={{ lineHeight: 2.2, fontWeight: "normal" }}
                      >
                        {tData?.getTrips[index].vehicle.registration}
                      </Paragraph>
                    </Col>
                    <Col span={8}>
                      {changeVehicle && (
                        <Select
                          style={{
                            fontWeight: "400",
                          }}
                          placeholder="New vehicle"
                          onChange={(val) => setNewVehicle(val)}
                        >
                          {rData?.getRoutes.map((route) => (
                            <OptGroup
                              key={route.id}
                              label={`${route.departure} - ${route.destination}`}
                            >
                              {route.vehicles
                                .filter(
                                  (vehicle) =>
                                    vehicle.id !==
                                    tData?.getTrips[index].vehicle.id
                                )
                                .map((vehicle) => (
                                  <Option key={vehicle.id} value={vehicle.id}>
                                    {vehicle.registration}
                                  </Option>
                                ))}
                            </OptGroup>
                          ))}
                        </Select>
                      )}
                    </Col>
                    <Col offset={4}>
                      {" "}
                      <Typography.Link
                        onClick={() => setChangeVehicle(!changeVehicle)}
                        style={{
                          whiteSpace: "nowrap",
                          fontWeight: "normal",
                          fontSize: "0.7rem",
                          float: "right",
                        }}
                      >
                        Change vehicle
                      </Typography.Link>
                    </Col>
                  </Row>
                </Form.Item>
                <Form.Item
                  label="Departure"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Row>
                    <Col span={8}>
                      <p style={{ fontWeight: "400" }}>Nairobi</p>
                      <p style={{ fontWeight: "400", fontSize: "0.7rem" }}>
                        {moment(
                          new Date(
                            parseInt(tData?.getTrips[index].departureTime) *
                              1000
                          )
                        ).format("MMM Do YYYY  |  h:mm a ")}
                      </p>
                    </Col>
                    <Col span={8}>
                      {changeDeparture && (
                        <TimePicker
                          use12Hours
                          style={{ width: "100%" }}
                          placeholder="New time"
                          onChange={(val) =>
                            setNewDepartureTime(String(val.unix()))
                          }
                        />
                      )}
                    </Col>
                    <Col offset={4}>
                      <Typography.Link
                        onClick={() => setChangeDeparture(!changeDeparture)}
                        style={{
                          whiteSpace: "nowrap",
                          fontWeight: "normal",
                          fontSize: "0.7rem",
                          float: "right",
                        }}
                      >
                        Change time
                      </Typography.Link>
                    </Col>
                  </Row>
                </Form.Item>
                <Form.Item
                  label="Destination"
                  style={{ fontWeight: "bold", margin: 0 }}
                >
                  <Row>
                    <Col span={8}>
                      <p style={{ fontWeight: "400" }}>Nairobi</p>
                      <p style={{ fontWeight: "400", fontSize: "0.7rem" }}>
                        {moment(
                          new Date(
                            Number(tData?.getTrips[index].departureTime) *
                              1000 +
                              tData?.getTrips[index].route.duration * 1000
                          )
                        ).format("MMM Do YYYY  |  h:mm a ")}
                      </p>
                    </Col>
                  </Row>
                </Form.Item>
              </Form>
              {(newDriver || newVehicle || newDepartureTime) && (
                <Button
                  type="primary"
                  block
                  style={{ marginTop: 12 }}
                  onClick={() => updateTrip(tData?.getTrips[index].id)}
                >
                  Save changes
                </Button>
              )}
            </TabPane>

            <TabPane tab="Bookings" key="2">
              <Row>
                <div style={{ maxHeight: 560, overflow: "scroll", width: 500 }}>
                  <Popconfirm
                    title="Ticket paid and booked is non-refundable. Continue?"
                    onConfirm={() => bookTicket(tData?.getTrips[index].id)}
                    okText="Book ticket"
                    cancelText="Back"
                  >
                    <Button style={{ marginBottom: 12 }} block type="primary">
                      Book ticket
                    </Button>
                  </Popconfirm>

                  <Row gutter={16}>
                    <Col span={12}>
                      <Card size="small">
                        <Statistic
                          title="Paid"
                          value={
                            tData?.getTrips[index].route.fare *
                            tData?.getTrips[index].bookings.filter(
                              (booking) => booking.payment?.mode
                            ).length
                          }
                          prefix="Ksh."
                          suffix={
                            <p style={{ color: "#808080", fontSize: "0.8rem" }}>
                              / Ksh.{" "}
                              {tData?.getTrips[index].route.fare *
                                tData?.getTrips[index].vehicle.seats}
                            </p>
                          }
                        />
                      </Card>
                    </Col>
                    <Col span={12}>
                      <Card size="small">
                        <Statistic
                          title="Passengers"
                          value={
                            tData?.getTrips[index].bookings.filter(
                              (booking) => booking.payment?.mode
                            ).length
                          }
                          suffix={
                            <p style={{ color: "#808080", fontSize: "0.8rem" }}>
                              /{tData?.getTrips[index].vehicle.seats}
                            </p>
                          }
                        />
                      </Card>
                    </Col>
                  </Row>
                  <VehiclePlan
                    seatsInfo={seatsInfo}
                    bookings={tData?.getTrips[index].bookings}
                  />
                </div>
                <div
                  style={{
                    width: "40%",
                    marginLeft: "5%",
                  }}
                >
                  <h3>Passenger list</h3>
                  <br />
                  <div
                    style={{
                      width: "40%",
                      marginLeft: "5%",
                      maxHeight: 350,
                      overflow: "scroll",
                    }}
                  >
                    {tData?.getTrips[index].bookings
                      .filter((booking) => booking.payment)
                      .map((booking, indx) => (
                        <p key={indx}>
                          {booking.seatNo}. {booking.passenger.name},{" "}
                          {booking.passenger.gender}, {booking.passenger.age}
                        </p>
                      ))}
                  </div>
                </div>
              </Row>
            </TabPane>
            <TabPane tab="Cancel & Refund" key="3">
              <Paragraph>
                <Alert message="Warning" banner />
                <br />
                Are you sure you want to cancel this trip permanently and issue
                refunds ? Type your passsword below to cancel trip.
              </Paragraph>
              <Input value={null} />

              <Button type="primary" block style={{ marginTop: 24 }}>
                Cancel trip
              </Button>
            </TabPane>
          </Tabs>
        ) : (
          <Empty />
        )}
      </Modal>
    </div>
  );
}

const VehiclePlan = ({ seatsInfo, bookings }) => {
  console.log(seatsInfo.filter((info) => info.seat == 1)[0]);
  console.log(bookings);

  const content = (
    <div>
      <strong>Steve Kinuthia , 21 , M</strong>
      <p style={{ margin: 0 }}>0748920306</p>
      <p style={{ margin: 0 }}>Id number : 38213846</p>
      <br />
      <Divider style={{ margin: 0 }} />
      <br />
      <p style={{ fontSize: "0.8rem" }}>
        <strong>Transaction</strong> : QPRSP0002W
      </p>
      <p style={{ fontSize: "0.8rem" }}>
        <strong>Paid on</strong> : {moment().format("MMM Do YYYY | h:mm a")}
      </p>
    </div>
  );
  const Seat = ({ num, style, info }) => {
    console.log(info);
    return (
      <Popover content={content} title="Seat Info">
        <button
          style={{
            background: "transparent",
            border: "none",
            outline: "none",
            ...style,
            position: "absolute",
          }}
        >
          <div
            style={{
              display: "flex",
              position: "relative",
              width: 40,
            }}
          >
            <div
              style={{
                width: 36,
                height: 36,
                background: info?.payment?.mode ? "#63C34A" : "skyblue",
                borderRadius: 8,
              }}
            >
              {num}
            </div>
            <div
              style={{
                height: 48,
                width: 12,
                background: "black",
                borderRadius: 12,
                position: "absolute",
                right: 0,
                top: -6,
              }}
            />
          </div>
        </button>
      </Popover>
    );
  };
  return (
    <>
      <div
        style={{
          width: 400,
          height: 250,
          position: "relative",
          marginTop: 24,
        }}
      >
        <Image
          alt="steering"
          src={"/steering.png"}
          width={48}
          height={48}
          style={{
            transform: "rotate(-90deg)",
            position: "absolute",
            left: 70,
            top: 30,
          }}
        />
        <Seat
          num={1}
          style={{ left: 70, bottom: 70 }}
          info={bookings.filter((booking) => booking.seatNo == "1")[0]}
        />
        <Seat
          num={2}
          style={{ left: 70, bottom: 0 }}
          info={bookings.filter((booking) => booking.seatNo == "2")[0]}
        />
        <Seat
          num={3}
          style={{ left: 140, bottom: 0 }}
          info={bookings.filter((booking) => booking.seatNo == "3")[0]}
        />
        <Seat
          num={4}
          style={{ left: 140, bottom: 70 }}
          info={bookings.filter((booking) => booking.seatNo == "4")[0]}
        />
        <Seat
          num={5}
          style={{ left: 140, bottom: 140 }}
          info={bookings.filter((booking) => booking.seatNo == "5")[0]}
        />
        <Seat
          num={6}
          style={{ left: 140, bottom: 210 }}
          info={bookings.filter((booking) => booking.seatNo == "6")[0]}
        />
        <Seat
          num={7}
          style={{ left: 210, bottom: 0 }}
          info={bookings.filter((booking) => booking.seatNo == "7")[0]}
        />
        <Seat
          num={8}
          style={{ left: 210, bottom: 140 }}
          info={bookings.filter((booking) => booking.seatNo == "8")[0]}
        />
        <Seat
          num={9}
          style={{ left: 210, bottom: 210 }}
          info={bookings.filter((booking) => booking.seatNo == "9")[0]}
        />
        <Seat
          num={10}
          style={{ left: 280, bottom: 0 }}
          info={bookings.filter((booking) => booking.seatNo == "10")[0]}
        />
        <Seat
          num={11}
          style={{ left: 280, bottom: 140 }}
          info={bookings.filter((booking) => booking.seatNo == "11")[0]}
        />
        <Seat
          num={12}
          style={{ left: 280, bottom: 210 }}
          info={bookings.filter((booking) => booking.seatNo == "12")[0]}
        />
        <Seat
          num={13}
          style={{ left: 350, bottom: 0 }}
          info={bookings.filter((booking) => booking.seatNo == "13")[0]}
        />
        <Seat
          num={14}
          style={{ left: 350, bottom: 70 }}
          info={bookings.filter((booking) => booking.seatNo == "14")[0]}
        />
        <Seat
          num={15}
          style={{ left: 350, bottom: 140 }}
          info={bookings.filter((booking) => booking.seatNo == "15")[0]}
        />
        <Seat
          num={16}
          style={{ left: 350, bottom: 210 }}
          info={bookings.filter((booking) => booking.seatNo == "16")[0]}
        />
      </div>
    </>
  );
};

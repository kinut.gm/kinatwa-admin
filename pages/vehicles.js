import {
  DownloadOutlined,
  InfoCircleOutlined,
  PlusCircleOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import {
  Row,
  Col,
  Button,
  Input,
  Typography,
  Table,
  Modal,
  Select,
  Result,
  Empty,
  Space,
  Statistic,
  DatePicker,
  Divider,
  Tabs,
  Card,
  Form,
  Alert,
  Tag,
  message,
  InputNumber,
} from "antd";
import moment from "moment";
import dynamic from "next/dynamic";

import { useRouter } from "next/router";
import { useQuery, useMutation } from "urql";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

const { Search } = Input;
const { Title, Text, Paragraph } = Typography;
const { OptGroup, Option } = Select;
const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

export default function Vehicles({ vehicles, routes, drivers }) {
  const [form] = Form.useForm();
  const router = useRouter();

  // States
  const [modal1Visible, setModal1Visible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [changeDriver, setChangeDriver] = useState(false);
  const [changeRoute, setChangeRoute] = useState(false);
  const [index, setIndex] = useState(-1);
  const [keyword, setKeyword] = useState("");
  const [password, setPassword] = useState("");

  const [owner, setOwner] = useState(null);
  const [driver, setDriver] = useState(null);
  const [route, setRoute] = useState(null);
  const [registration, setRegistration] = useState("");
  const [seats, setSeats] = useState(null);

  const [newDriver, setNewDriver] = useState("");
  const [newRoute, setNewRoute] = useState("");

  // Requests
  const ADD_VEHICLE = `
    mutation ADD_VEHICLE(
      $registration: String
      $driver: ID
      $route: ID
      $owner: ID
      $seats: Int
    ) {
      addVehicle(
        driver: $driver
        registration: $registration
        route: $route
        owner: $owner
        seats: $seats
      ) {
        id
        registration
      }
    }
  `;

  const GET_VEHICLES = `
  query {
    getVehicles {
      id
      registration
      primaryRoute {
        id
        departure
        destination
      }
      owner{
        id
        name
      }
      removed
      driver {
        id
        name
        phoneNumber
      }
      createdAt
      addedBy {
        name
      }
      trips {
        journeyType
        departureTime
        route {
          departure
          destination
          duration
          fare
        }
      }
    }
  }
  `;
  const GET_ROUTES = `
  query {
    getRoutes {
      id
      destination
      departure
      fare
    }
  }
  `;
  const GET_ADMINS = `
  query {
    getAdmins {
      id
      name
      username
      role
    }
  }
  `;
  const UPDATE_VEHICLE = `
    mutation UPDATE_VEHICLE($id:ID, $driver: ID, $primaryRoute: ID , $removed: Boolean){
      updateVehicle(id:$id, driver:$driver, primaryRoute:$primaryRoute, removed:$removed){
        id
        removed
        driver{
          name
        }
      }
    }
  `;

  const [addVehicleResult, _addVehicle] = useMutation(ADD_VEHICLE);

  const [updateVehicleResult, _updateVehicle] = useMutation(UPDATE_VEHICLE);

  const [{ data: vData, fetching: vFetching, error: vError }, reexecuteQuery] =
    useQuery({
      query: GET_VEHICLES,
    });

  const [{ data: rData, fetching: rFetching, error: rError }] = useQuery({
    query: GET_ROUTES,
  });

  const [{ data: aData, fetching: aFetching, error: aError }] = useQuery({
    query: GET_ADMINS,
  });

  console.log(rData);

  // Functions
  const addVehicle = () => {
    console.log({
      driver,
      owner,
      route,
      registration,
      seats,
    });
    _addVehicle({
      driver,
      owner,
      route,
      registration,
      seats,
    })
      .then(() => {
        message.success(
          `Vehicle '${registration}' has been added to the system`
        );
      })
      .then(() => {
        setDriver(null);
        setOwner(null);
        setRegistration(null);
        setRoute(null);
        setModal1Visible(false);
        reexecuteQuery({ requestPolicy: "network-only" });
      })
      .catch((err) => message.error("Failed! Something gross happened"));
  };

  const updateVehicle = (id) => {
    _updateVehicle({
      id,
      driver: newDriver ? newDriver : vData?.getVehicles[index].driver.id,
      primaryRoute: newRoute
        ? newRoute
        : vData?.getVehicles[index].primaryRoute.id,
    })
      .then((result) => {
        message.success("Changes made successfully!");
        setModal2Visible(false);
        setNewDriver("");
        setNewRoute("");
        setChangeDriver(false);
        setChangeRoute(false);
        reexecuteQuery({ requestPolicy: "network-only" });
      })
      .catch((err) => message.error("An unexpected error occured"));
  };

  const deleteVehicle = (id) => {
    console.log({ id, removed: true });
    _updateVehicle({
      id,
      removed: true,
    })
      .then((result) => {
        message.success("Vehicle removed!");
        setModal2Visible(false);
        setNewDriver("");
        setNewRoute("");
        setChangeDriver(false);
        setChangeRoute(false);
        reexecuteQuery({ requestPolicy: "network-only" });
      })
      .catch((err) => message.error("An unexpected error occured"));
  };

  // Configs
  let options3 = {
    chart: {
      type: "area",
      stacked: false,
      height: 350,
      zoom: {
        type: "x",
        enabled: true,
        autoScaleYaxis: true,
      },
      toolbar: {
        autoSelected: "zoom",
      },
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
    },
    title: {
      text: "Stock Price Movement",
      align: "left",
    },
    fill: {
      type: "gradient",
      gradient: {
        shadeIntensity: 1,
        inverseColors: false,
        opacityFrom: 0.5,
        opacityTo: 0,
        stops: [0, 90, 100],
      },
    },
    yaxis: {
      labels: {
        formatter: function (val) {
          return (val / 1000000).toFixed(0);
        },
      },
      title: {
        text: "Price",
      },
    },
    xaxis: {
      type: "datetime",
    },
    tooltip: {
      shared: false,
      y: {
        formatter: function (val) {
          return (val / 1000000).toFixed(0);
        },
      },
    },
  };

  let series3 = [
    {
      name: "XYZ MOTORS",
      data: [],
    },
  ];

  const columns = [
    {
      title: "Registration",
      dataIndex: "registration",
    },
    {
      title: "Primary route",
      dataIndex: "",
      render: (record) => {
        return (
          <Paragraph>{`${record?.primaryRoute?.departure} - ${record?.primaryRoute?.destination} `}</Paragraph>
        );
      },
    },
    {
      title: "Owner",
      dataIndex: "",
      sortDirections: ["descend"],
      render: (record) => {
        return <Paragraph>{record?.owner?.name}</Paragraph>;
      },
    },
    {
      title: "Driver",
      dataIndex: "",
      render: (record) => {
        return <Paragraph>{record?.driver?.name}</Paragraph>;
      },
    },
    {
      title: "Added on",
      dataIndex: "",
      render: (record) => {
        return (
          <Paragraph>
            {new Date(parseInt(record.createdAt)).toDateString()}
          </Paragraph>
        );
      },
    },
    {
      title: "",
      dataIndex: "",
      render: (record) => {
        return (
          <Button
            type="link"
            onClick={() => {
              setIndex(vData?.getVehicles.indexOf(record));
              console.log(vData?.getVehicles.indexOf(record));
              setModal2Visible(true);
            }}
          >
            More
          </Button>
        );
      },
    },
  ];

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  if (vFetching) return <p>Loading...</p>;
  if (vError) return <p>Error...</p>;

  return (
    <div
      style={{
        padding: 24,
        minHeight: 360,
        background: "#fff",
      }}
    >
      <Row>
        <Col lg={12} sm={24}>
          <Button type="primary" onClick={() => setModal1Visible(true)}>
            {" "}
            <PlusCircleOutlined /> Add a new vehicle
          </Button>
        </Col>
        <Col lg={12} sm={24}>
          <Input
            suffix={<SearchOutlined />}
            placeholder="Search registration, owner"
            onChange={(e) => setKeyword(e.target.value)}
            style={{
              width: 300,
              float: "right",
            }}
          />
        </Col>
      </Row>
      <Row style={{ marginTop: 12 }}>
        <Title level={2}>
          {
            vData?.getVehicles.filter(
              (vehicle) =>
                !vehicle.removed &&
                (vehicle.registration
                  .toLowerCase()
                  .includes(keyword.toLowerCase()) ||
                  vehicle.owner.name
                    .toLowerCase()
                    .includes(keyword.toLowerCase()))
            ).length
          }
        </Title>
        <Text style={{ lineHeight: 3, marginLeft: 8 }}>vehicles</Text>
      </Row>
      {vData?.getVehicles.filter((vehicle) => !vehicle.removed).length == 0 ? (
        <Result
          icon={<Empty />}
          title="Set up a vehicle to get started!"
          extra={
            <Button type="primary" onClick={() => setModal1Visible(true)}>
              Add vehicle
            </Button>
          }
        />
      ) : (
        <Table
          size="small"
          pagination={false}
          scroll={{
            y: "calc(100vh - 350px)",
          }}
          columns={columns}
          dataSource={vData?.getVehicles.filter(
            (vehicle) =>
              !vehicle.removed &&
              (vehicle.registration
                .toLowerCase()
                .includes(keyword.toLowerCase()) ||
                vehicle.owner.name
                  .toLowerCase()
                  .includes(keyword.toLowerCase()))
          )}
        />
      )}

      {/* Add vehicle modal */}
      <Modal
        visible={modal1Visible}
        okText="Add vehicle"
        onOk={addVehicle}
        onCancel={() => {
          setDriver(null);
          setOwner(null);
          setRegistration(null);
          setRoute(null);
          setModal1Visible(false);
        }}
      >
        <br />
        <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
          Add a new vehicle
        </Title>
        <br />
        <Form {...formItemLayout} form={form}>
          <Form.Item
            label="Primary route"
            style={{ fontWeight: "bold", margin: 0 }}
          >
            <Select
              style={{
                width: "100%",
                marginBottom: 16,
                fontWeight: "400",
              }}
              onChange={(val) => setRoute(val)}
              placeholder="Primary route"
            >
              {rData?.getRoutes.map((route) => {
                return (
                  <Option value={route.id} key={route.id}>
                    {route.departure} - {route.destination}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Driver" style={{ fontWeight: "bold", margin: 0 }}>
            <Select
              style={{
                width: "100%",
                marginBottom: 16,
                fontWeight: 400,
                display: "block",
              }}
              onChange={(val) => setDriver(val)}
              placeholder="Driver"
            >
              <OptGroup label="Drivers">
                {aData?.getAdmins
                  .filter((admin) => admin.role.includes("driver"))
                  .map((driver) => {
                    return (
                      <Option value={driver.id} key={driver.id}>
                        {driver.name}
                      </Option>
                    );
                  })}
              </OptGroup>
            </Select>
          </Form.Item>
          <Form.Item
            label="Registration"
            style={{ fontWeight: "bold", margin: 0 }}
          >
            <Input
              placeholder="ex. KCB 519L"
              block
              onChange={(e) => setRegistration(e.target.value)}
            />
          </Form.Item>
          <br />
          <Form.Item label="Seats" style={{ fontWeight: "bold", margin: 0 }}>
            <InputNumber
              placeholder="Seats"
              min={1}
              style={{ width: "100%" }}
              max={70}
              block
              onChange={(val) => setSeats(val)}
            />
          </Form.Item>
          <br />
          <Form.Item label="Owner" style={{ fontWeight: "bold", margin: 0 }}>
            <Select
              style={{
                width: "100%",
                fontWeight: 400,
              }}
              onChange={(val) => setOwner(val)}
              placeholder="Owner"
            >
              <OptGroup label="Vehicle owners">
                {aData?.getAdmins
                  .filter((admin) => admin.role.includes("vehicle_owner"))
                  .map((driver) => {
                    return (
                      <Option value={driver.id} key={driver.id}>
                        {driver.name}
                      </Option>
                    );
                  })}
              </OptGroup>
            </Select>
          </Form.Item>
        </Form>
      </Modal>

      {/* More btn modal */}
      <Modal
        visible={modal2Visible}
        footer={null}
        onOk={null}
        onCancel={() => setModal2Visible(false)}
      >
        {index > -1 && (
          <>
            <br />
            <Title level={2} style={{ color: "#63C34A", letterSpacing: -1 }}>
              {vData?.getVehicles[index].registration}
            </Title>
            <br />

            <Tabs>
              <TabPane tab="Info" key="1">
                <Form {...formItemLayout} form={form}>
                  <Form.Item
                    label="Driver"
                    style={{ fontWeight: "bold", margin: 0 }}
                  >
                    <Row>
                      <Col span={8}>
                        {" "}
                        <Paragraph
                          style={{ lineHeight: 2.2, fontWeight: "normal" }}
                        >
                          {vData?.getVehicles[index].driver.name}
                        </Paragraph>
                      </Col>

                      <Col span={8}>
                        {changeDriver && (
                          <Select
                            style={{
                              fontWeight: "400",
                            }}
                            placeholder="New driver"
                            onChange={(val) => setNewDriver(val)}
                            dropdownRender={(menu) => (
                              <>
                                {menu}
                                <Divider style={{ margin: "8px 0" }} />
                                <Space
                                  align="center"
                                  style={{ padding: "0px 8px 8px" }}
                                >
                                  <Typography.Link
                                    onClick={() => setChangeDriver(true)}
                                    style={{ whiteSpace: "nowrap" }}
                                  >
                                    <PlusOutlined /> Add new driver
                                  </Typography.Link>
                                </Space>
                              </>
                            )}
                          >
                            {aData?.getAdmins
                              .filter(
                                (admin) =>
                                  admin.role.includes("driver") &&
                                  admin.name !==
                                    vData?.getVehicles[index].driver.name
                              )
                              .map((driver) => (
                                <Option key={driver.id} value={driver.id}>
                                  {driver.name}
                                </Option>
                              ))}
                          </Select>
                        )}
                      </Col>

                      <Col offset={4}>
                        {" "}
                        <Typography.Link
                          onClick={() => setChangeDriver(!changeDriver)}
                          style={{
                            whiteSpace: "nowrap",
                            fontWeight: "normal",
                            fontSize: "0.7rem",
                            float: "right",
                          }}
                        >
                          Change driver
                        </Typography.Link>
                      </Col>
                    </Row>
                  </Form.Item>
                  <Form.Item
                    label="Primary route"
                    style={{ fontWeight: "bold", margin: 0 }}
                  >
                    <Row>
                      <Col span={8}>
                        {" "}
                        <Paragraph
                          style={{ lineHeight: 2.2, fontWeight: "normal" }}
                        >
                          {vData?.getVehicles[index].primaryRoute.departure} -{" "}
                          {vData?.getVehicles[index].primaryRoute.destination}
                        </Paragraph>
                      </Col>
                      <Col span={8}>
                        {changeRoute && (
                          <Select
                            style={{
                              fontWeight: "400",
                            }}
                            placeholder="New route"
                            onChange={(val) => setNewRoute(val)}
                          >
                            {rData?.getRoutes
                              .filter(
                                (route) =>
                                  route.id !==
                                  vData?.getVehicles[index].primaryRoute.id
                              )
                              .map((route) => (
                                <Option key={route.id} value={route.id}>
                                  {route.departure} - {route.destination}
                                </Option>
                              ))}
                          </Select>
                        )}
                      </Col>
                      <Col offset={4}>
                        {" "}
                        <Typography.Link
                          onClick={() => setChangeRoute(!changeRoute)}
                          style={{
                            whiteSpace: "nowrap",
                            fontWeight: "normal",
                            fontSize: "0.7rem",
                            float: "right",
                          }}
                        >
                          Change route
                        </Typography.Link>
                      </Col>
                    </Row>
                  </Form.Item>

                  <Form.Item
                    label="Added on"
                    style={{ fontWeight: "bold", margin: 0 }}
                  >
                    <Paragraph
                      style={{ lineHeight: 2.2, fontWeight: "normal" }}
                    >
                      {new Date(
                        parseInt(vData?.getVehicles[index].createdAt)
                      ).toDateString()}
                    </Paragraph>
                  </Form.Item>
                </Form>
                {(newDriver || newRoute) && (
                  <Button
                    type="primary"
                    block
                    style={{ marginTop: 12 }}
                    onClick={() => updateVehicle(vData?.getVehicles[index].id)}
                  >
                    Save changes
                  </Button>
                )}
              </TabPane>

              <TabPane tab="Trips & Earnings" key="2">
                <Row gutter={10}>
                  <Col span={16}>
                    <RangePicker
                      placeholder={["Start date", "End date"]}
                      style={{ marginBottom: 12, width: "100%" }}
                    />
                  </Col>
                  <Col span={8}>
                    <Select placeholder="ex. Today" style={{ width: "100%" }}>
                      <Option value="today">Today</Option>
                    </Select>
                  </Col>
                </Row>

                <Row gutter={16}>
                  <Col span={8}>
                    <Card size="small">
                      <Statistic
                        title="Trips"
                        value={vData?.getVehicles[index].trips.length}
                      />
                    </Card>
                  </Col>
                  <Col span={8}>
                    <Card size="small">
                      <Statistic title="Earnings" value={34} />
                    </Card>
                  </Col>
                  <Col span={8}>
                    <Card size="small">
                      <Statistic title="Expenditure" value={34} />
                    </Card>
                  </Col>
                </Row>
                <Row>
                  <Col span={16}>
                    <Divider orientation="left">Trips</Divider>
                  </Col>
                  <Col span={8}>
                    <Button size="small" style={{ marginTop: 16 }}>
                      <DownloadOutlined /> Download report
                    </Button>
                  </Col>
                </Row>

                <div
                  style={{
                    maxHeight: 200,
                    overflow: "scroll",
                    padding: "12px 0px",
                  }}
                >
                  {[
                    {
                      availableSeats: 12,
                    },
                    {
                      availableSeats: 0,
                    },
                    {
                      availableSeats: 12,
                    },
                    {
                      availableSeats: 0,
                    },
                  ].map((trip) => (
                    <TripBreakdown key={trip.availableSeats} trip={trip} />
                  ))}
                </div>
              </TabPane>
              <TabPane tab="Stats" key="3">
                <Chart
                  options={options3}
                  series={series3}
                  type="area"
                  width={500}
                  height={320}
                />
              </TabPane>
              <TabPane tab="Delete" key="4">
                <Paragraph>
                  <Alert message="Warning" banner />
                  <br />
                  Are you sure you want to remove this vehicle permanently? All
                  records associated with this vehicle will still remain. Type
                  your password below to complete deletion.
                </Paragraph>
                <Input.Password
                  block
                  onChange={(e) => setPassword(e.target.value)}
                />

                <Button
                  block
                  type="primary"
                  style={{ marginTop: 12 }}
                  onClick={() => deleteVehicle(vData.getVehicles[index].id)}
                >
                  Delete vehicle
                </Button>
              </TabPane>
            </Tabs>
          </>
        )}
      </Modal>
    </div>
  );
}

const TripBreakdown = ({ trip }) => {
  return (
    <Row
      style={{
        marginBottom: 12,
        paddingBottom: 10,
        borderBottom: "#f1f1f1 0.5px solid",
      }}
    >
      <Col span={7}>
        <Title level={4} style={{ margin: 0 }}>
          NRB - MSA
        </Title>
        {moment().format("MMM Do YYYY")}
      </Col>
      <Col span={7}>
        {moment().format(" h:mm a")} - {moment().format(" h:mm a")}{" "}
      </Col>
      <Col span={4}>
        <Tag
          color={
            trip.availableSeats == 0
              ? "green"
              : trip.availableSeats < 5
              ? "orange"
              : trip.availableSeats > 9
              ? "blue"
              : "gray"
          }
        >
          {trip.availableSeats == 0 ? "FULL" : `${trip.availableSeats} LEFT`}
        </Tag>
      </Col>
      <Col span={6}>
        <Title level={5}>Ksh. 24,000</Title>
      </Col>
    </Row>
  );
};

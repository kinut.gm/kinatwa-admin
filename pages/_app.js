import "../styles/globals.css";

import { useState, useContext } from "react";
import { createClient, Provider } from "urql";
import styles from "../styles/Styles.module.css";
import { createContext } from "react";

import {
  Layout,
  Menu,
  Button,
  Avatar,
  Form,
  Input,
  Alert,
  message,
  Popconfirm,
} from "antd";
import { useRouter } from "next/router";
import { useQuery, useMutation } from "urql";

const { Header, Content, Footer, Sider } = Layout;

const client = createClient({
  url: "https://kinatwa-backend.herokuapp.com/graphql",
});

export const AuthContext = createContext();

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState(null);

  return (
    <AuthContext.Provider value={[user, setUser]}>
      <Provider value={client}>
        <Theme>
          <Component {...pageProps} />
        </Theme>
      </Provider>
    </AuthContext.Provider>
  );
}

const Theme = ({ children }) => {
  const [user, setUser] = useContext(AuthContext);
  const router = useRouter();

  if (!user) return <Login setUser={(val) => setUser(val)} />;

  return (
    <Layout style={{ height: "100vh" }}>
      <Sider
        width={250}
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
        breakpoint="lg"
        collapsedWidth="0"
        // onBreakpoint={(broken) => {
        //   console.log(broken);
        // }}
        // onCollapse={(collapsed, type) => {
        //   console.log(collapsed, type);
        // }}
      >
        <div
          className={styles.logo}
          style={{
            height: 32,
            margin: 16,
            background: "rgba(255, 255, 255, 0.2)",
          }}
        />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key={"1"} onClick={() => router.push("/")}>
            Dashboard
          </Menu.Item>
          <Menu.Item key={"2"} onClick={() => router.push("/trips&bookings")}>
            Trips & Bookings
          </Menu.Item>
          <Menu.Item key={"3"} onClick={() => router.push("/vehicles")}>
            Vehicles
          </Menu.Item>
          <Menu.Item key={"4"} onClick={() => router.push("/route")}>
            Routes
          </Menu.Item>
          <Menu.Item key={"5"} onClick={() => router.push("/users&access")}>
            Users & Access
          </Menu.Item>
          <Button
            block
            type="link"
            onClick={() => router.push("/profile")}
            style={{
              textAlign: "left",
              color: "#63C34A",
              background: "transparent",
              marginTop: "calc(70vh - 232px)",
            }}
          >
            Profile
          </Button>
          <Popconfirm
            title="Are you sure?"
            onConfirm={() => {
              message.info("Logged out");
              setUser(null);
            }}
          >
            <Button
              type="link"
              onClick={() => console.log("profile")}
              style={{
                color: "#63C34A",
                marginTop: 6,
              }}
            >
              Sign out
            </Button>
          </Popconfirm>
        </Menu>
      </Sider>
      <Layout
        className="site-layout"
        style={{
          marginLeft: 250,
        }}
      >
        <Header
          style={{
            padding: 0,
            background: "#fff",
            width: "100%",
            position: "sticky",
            top: 0,
            zIndex: 99,
          }}
        >
          <p style={{ float: "right", marginRight: 12 }}>
            Welcome <strong>{user.name}</strong>{" "}
            <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
              S
            </Avatar>
          </p>
        </Header>
        <Content
          style={{
            margin: "24px 16px 0",
            overflow: "scroll",
            maxHeight: 700,
          }}
        >
          {children}
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          Designed & Developed by{" "}
          <a
            style={{ textDecoration: "underline" }}
            href="http://ewt.vercel.app"
          >
            Everything With Tech (EWT)
          </a>{" "}
          ©{new Date().getFullYear().toString()}
        </Footer>
      </Layout>
    </Layout>
  );
};

function Login({ setUser }) {
  const [form] = Form.useForm();

  // States
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  // Requests
  const GET_ADMINS = `
    query{
      getAdmins{
        id
        name
        role
        username
        password
      }
    }
  `;

  const ADD_SUPER_ADMIN = `
    mutation ADD_SUPER_ADMIN($username:String,$password:String,$name:String, $role: String!){
        addAdmin(username:$username, password:$password, name:$name , role:$role){
          username
          name
          id
        }
    }
  `;

  const [{ data, fetching, error }] = useQuery({ query: GET_ADMINS });
  const [addAdminResult, _addAdmin] = useMutation(ADD_SUPER_ADMIN);

  // Functions
  const handleLogin = () => {
    if (username === "superadmin" && password === "12kinatwasacco90") {
      // Create a superadmin
      if (
        data?.getAdmins.filter((admin) => admin.role.includes("superadmin"))
          .length > 0
      ) {
        setUser({
          id: data?.getAdmins.filter((admin) =>
            admin.role.includes("superadmin")
          )[0].id,
          name: data?.getAdmins.filter((admin) =>
            admin.role.includes("superadmin")
          )[0].name,
        });
      } else {
        _addAdmin({
          username: "superadmin",
          role: "superadmin",
          password: "12kinatwasacco90",
          name: "SuperAdmin",
        })
          .then((res) => {
            message.success("SuperAdmin created");
            setUser({
              id: res.data?.addAdmin?.id,
              name: "SuperAdmin",
            });
          })
          .catch((err) => message.error(err.message));
      }
    } else if (!username || !password) {
      message.warning("Missing credentials. Please try again");
      return;
    } else {
      if (
        data?.getAdmins.filter(
          (admin) => admin.username == username && admin.password == password
        ).length > 0
      ) {
        message.success("Logged in!");
        setUser({
          id: data?.getAdmins.filter(
            (admin) => admin.username == username && admin.password == password
          )[0].id,
          name: data?.getAdmins.filter(
            (admin) => admin.username == username && admin.password == password
          )[0].name,
        });
      } else {
        console.log(data?.getAdmins);
        message.error("Credentials invalid. Please try again");
      }
    }
  };

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  return (
    <>
      <div style={{ background: "#63C34A", height: "100vh", width: "100vw" }}>
        <Form
          {...formItemLayout}
          form={form}
          style={{
            background: "#fff",
            width: "400px",
            padding: 24,
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%,-50%)",
          }}
        >
          <Alert
            message="Welcome to Kinatwa Sacco Admin portal"
            type="info"
            showIcon
            banner
          />
          <br />
          <Form.Item label="Username" style={{ fontWeight: "bold", margin: 0 }}>
            <Input onChange={(e) => setUsername(e.target.value)} />
          </Form.Item>

          <Form.Item
            label="Password"
            style={{ fontWeight: "bold", margin: 0, marginTop: 12 }}
          >
            <Input.Password onChange={(e) => setPassword(e.target.value)} />
          </Form.Item>
          <Button
            type="primary"
            block
            style={{ marginTop: 36 }}
            onClick={handleLogin}
          >
            Login
          </Button>
        </Form>
      </div>
    </>
  );
}

export default MyApp;
